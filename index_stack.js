import {Navigation} from "react-native-navigation";

import HomeScreen from './src/screens/HomeScreen'
import RegisterScreen from './src/screens/RegisterScreen'

Navigation.registerComponent("Home", () => HomeScreen)
Navigation.registerComponent("Register", () => RegisterScreen)

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            stack: {
                children: [
                    {
                        component: {
                            name: 'Home'
                        },
                    }
                ]
            }
        }
    })
})

