import React, {Component} from 'react'
import {View, StyleSheet} from 'react-native'
import {Container, Content, Text, Button, Thumbnail} from "native-base";
import ImagePicker from 'react-native-image-picker';
import  AsynStroage from '@react-native-community/async-storage'
const axios = require('axios');

const options = {
    title: 'Choose an Image',
    storageOptions: {
        skipBackup: true,
        path: 'images',
    },
};


class Home extends Component {


    state = {
        imageSource: ''
    }

    writeData = async () => {

        const user = {
            name: 'Saiyai',
            age: 22,
            gender: 'f'
        }

        try {
            await AsynStroage.setItem("@UP_Student", JSON.stringify(user))
            console.log("===> success")
        } catch (e) {
            console.log("error aii...")
        }
    }

    mergData = async () => {

        const user = {
           dob: "01-01-2010"

        }

        try {
            await AsynStroage.mergeItem("@UP_Student", JSON.stringify(user))
            console.log("===> success")
        } catch (e) {
            console.log("error aii...")
        }
    }

    getData = async () => {
        try {
            const studentName = await AsynStroage.getItem("@UP_Student")
            console.log("===> Student: ", studentName)

        } catch (e) {
            console.log("error...")
        }
    }



    componentDidMount() {
        // this.getData()
        // this.writeData()
        // this.mergData()
        // this.getData()
    }


    //========== API ==========

    getArticles = () => {
        axios.get("http://www.api-ams.me/v1/api/articles?page=1&limit=15")
            .then((result) => {
                // success blog
                

            })
            .catch((error) => {
                // error blog
                console.log("> error", error)
            })
    }


    getPhoto = () => {

        // Open Image Library:
        ImagePicker.launchImageLibrary(options, (response) => {
            const source = {uri: response.uri};
            console.log("==> ", source)


            this.setState({
                imageSource: source,
            });
        });


        // ImagePicker.showImagePicker(options, (response) => {
        //     console.log('Response = ', response);
        //
        //     if (response.didCancel) {
        //         console.log('User cancelled image picker');
        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //         const source = { uri: response.uri };
        //         console.log("==> ", source)
        //
        //
        //         this.setState({
        //             imageSource: source,
        //         });
        //     }
        // });
    }

    render() {
        const {containerStyle} = styles

        return (
            <Container>
                <Content>
                    <Text>Image</Text>
                    <Thumbnail square large source={this.state.imageSource}/>
                    <View style={styles.buttonStyle}>
                        <Button onPress={this.getPhoto}>
                            <Text>Photo</Text>
                        </Button>
                    </View>

                </Content>
            </Container>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    },
    buttonStyle: {
        flex: 1,
        alignSelf: 'center'
    }
})

export default Home