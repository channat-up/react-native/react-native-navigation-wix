import React, {Component} from 'react'
import {View, Text, StyleSheet } from 'react-native'
import {Button} from "native-base";
import {Navigation} from "react-native-navigation";

class SideMenu extends Component {

    goToScreen = (screenName) => {
        // hide left menu
        Navigation.mergeOptions(this.props.componentId, {
            sideMenu: {
                left: {
                    visible: false
                }
            }
        })
        // navigate to another screen
        Navigation.push("CenterScreen", {
            component: {
                name: screenName
            }
        })


    }


    render() {
        const {containerStyle} = styles

        return (
            <View style={containerStyle}>
                <Text> SideMenu </Text>
                <Button onPress={() => this.goToScreen("SignUp") }>
                    <Text>Sign Up</Text>
                </Button>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    }
})

export default SideMenu 