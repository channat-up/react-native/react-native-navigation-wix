import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'

class TabScreenB extends Component {

    render() {
        const {containerStyle} = styles

        return (
            <View style={containerStyle}>
                <Text> TabScreenB </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    }
})

export default TabScreenB 