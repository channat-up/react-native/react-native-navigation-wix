import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'

class SignUp extends Component {

    render() {
        const {containerStyle} = styles

        return (
            <View style={containerStyle}>
                <Text> SignUp </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    }
})

export default SignUp 