import React, {Component} from 'react'
import {View, Text, StyleSheet, Button} from 'react-native'
import {Navigation} from "react-native-navigation";


class ScreenStack extends Component {

    goToScreen = (screenName) => {
        Navigation.setRoot({
            root: {
                bottomTabs: {
                    children: [
                        {
                            component: {
                                name: "TabScreenA",
                                options: {
                                    bottomTab: {
                                        text: "Tab A"
                                    }
                                }
                            },

                        },
                        {
                            component: {
                                name: "TabScreenB",
                                options: {
                                    bottomTab: {
                                        text: "Tab B"
                                    }
                                }
                            },

                        },
                    ]

                }
            }
        })
    }

    render() {
        const {containerStyle} = styles

        return (
            <View style={containerStyle}>
                <Text> ScreenStack </Text>
                <Button title={"Go new Screen"} onPress={() => this.goToScreen("TabScreenA")} />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    }
})

export default ScreenStack 