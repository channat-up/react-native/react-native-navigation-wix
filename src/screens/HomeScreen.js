import React, {Component} from 'react'
import {View,Text, StyleSheet} from 'react-native'

import { Button } from "native-base";
import {Navigation} from "react-native-navigation";

var toggle = false;

class HomeScreen extends Component {

    static options() {
        return {
            topBar: {
                title: {
                    text: "Home Screen"
                },
                visible: true,
                leftButtons: {
                    id: "sideMenu",
                    icon: require("../icons/icon.png")
                }

            }
        }
    }

    constructor(props) {
        super(props);
        Navigation.events().bindComponent(this);
    }


    goToScreen = (screenName) => {
        Navigation.push(this.props.componentId, {
            component: {
                name: screenName
            }
        })
    }


    navigationButtonPressed = ({componentID}) => {
        toggle = !toggle;
        Navigation.mergeOptions(this.props.componentId,{
            sideMenu: {
                left: {
                    visible: toggle
                }
            }
        })
    }

    render() {
        const {containerStyle} = styles

        return (
            <View style={containerStyle}>
                <Button onPress={() => this.goToScreen("Register")}>
                    <Text>Register</Text>
                </Button>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50,
        justifyContent: 'center',
        alignSelf: 'center'
    }
})

export default HomeScreen 