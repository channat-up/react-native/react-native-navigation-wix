import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'
import classPrivateFieldGet from "@babel/runtime/helpers/esm/classPrivateFieldGet";
const axios = require('axios');

class APIScreen extends Component {


    state = {
        articles: null
    }


    componentDidMount(): void {

        axios.get("http://www.api-ams.me/v1/api/articles?page=1&limit=15")
            .then(result => {
                // success blog
                console.log("===> result", result.data.DATA)
                this.setState({
                    articles: result.data.DATA
                })
            })

            .catch(error => {
                // error blog
                console.log("error", error)
            })

        axios.post("http://www.api-ams.me/v1/api/articles", {
            "TITLE": "Ok ",
            "DESCRIPTION": "Take a brake"
        })
            .then(res => console.log("success"))
            .catch(err => console.log("Error"))

    }

    render() {
        const {containerStyle} = styles

        var renderData = this.state.articles ? (
            this.state.articles.map((article, key) => (
                <Text key={key}>{article.TITLE}</Text>
            ))
        ) : (
           <View>
               {console.log("nothing")}
               <Text >Empty</Text>
           </View>

        )

        return (
            <View style={containerStyle}>
                {renderData}
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    }
})

export default APIScreen 