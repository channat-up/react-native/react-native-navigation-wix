import React, {Component} from 'react'
import {View, Text, StyleSheet} from 'react-native'

class RegisterScreen extends Component {

    static options() {
        return {
            topBar: {
                title: {
                    text: "Register"
                },
                visible: true
            }
        }
    }

    render() {
        const {containerStyle} = styles

        return (
            <View style={containerStyle}>
                <Text> RegisterScreen </Text>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 50
    }
})

export default RegisterScreen 