import {Navigation} from "react-native-navigation";

import ScreenStack from './src/screens/ScreenStack'
import TabScreenA from './src/screens/TabScreenA'
import TabScreenB from './src/screens/TabScreenB'

Navigation.registerComponent("ScreenStack", () => ScreenStack);
Navigation.registerComponent("TabScreenA", () => TabScreenA);
Navigation.registerComponent("TabScreenB", () => TabScreenB);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            stack: {
                children: [
                    {
                        component: {
                            name: "ScreenStack",
                        }
                    }
                ]
            }
        }
    })
})