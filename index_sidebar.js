import {Navigation} from "react-native-navigation";

import HomeScreen from './src/screens/HomeScreen'
import SideMenu from './src/screens/SideMenu'
import RegisterScreen from "./src/screens/RegisterScreen";
import SignUp from './src/screens/SignUp'

// Register component

Navigation.registerComponent("Home", () => HomeScreen);
Navigation.registerComponent("SideMenu", () => SideMenu);
Navigation.registerComponent("Register", () => RegisterScreen);
Navigation.registerComponent("SignUp", () => SignUp);

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            sideMenu: {
                left: {
                    id: "sideMenu",
                    component: {
                        name: 'SideMenu'
                    }
                },
                center: {
                   stack: {
                       id: "CenterScreen",
                       children:[{
                           component: {
                               name: "Home"
                           }
                       }]
                   }
                },

            }
        }
    })
})


