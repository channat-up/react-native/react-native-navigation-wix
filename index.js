import {Navigation} from "react-native-navigation";

import Home from './src/screen_hardward/Home'
import APIScreen from './src/screens/APIScreen'

Navigation.registerComponent("Home", () => Home)
Navigation.registerComponent("APIScreen", () => APIScreen)

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            component: {
                name: "APIScreen"
            }
        }
    })
})