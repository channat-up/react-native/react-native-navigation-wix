
import {Navigation} from "react-native-navigation";

import HomeScreen from './src/screens/HomeScreen'
import SignUp from './src/screens/SignUp'

Navigation.registerComponent("Home", ()=> HomeScreen)
Navigation.registerComponent("SignUp", () => SignUp)

Navigation.events().registerAppLaunchedListener(() => {
    Navigation.setRoot({
        root: {
            bottomTabs: {
                children: [
                    {
                         component: {
                             name: 'SignUp',
                             options: {
                                bottomTab: {
                                    text: 'SignUp',
                                    icon: require('./src/icons/signup.png'),
                                    selectedIcon: require('./src/icons/signup_selected.png'),
                                    selectedColor: 'red'
                                }
                             }
                         }
                    },
                    {
                        component: {
                            name: "Home",
                            options: {
                                bottomTab: {
                                    text: 'Home',
                                    icon: require('./src/icons/home.png')
                                }
                            }
                        }
                    }
                ]
            }
        }
    })
})